/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package arraylist;
import java.util.*; 
/**
 *
 * @author pertuniatladi
 */
public class ArrayListExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int[] primes = {2, 3, 5, 7, 11, 13, 17};
        String[] names = {"john", "Johnny", "Tom", "Harry"};
        
        //Declare ArrayList of String objects 
        ArrayList<String> cities = new ArrayList<>(
                              Arrays.asList("London", "Tokyo", "New York"));

        ArrayList<Float> floats = new ArrayList<>(Arrays.asList(3.14f, 6.28f, 9.56f));
        
        //declare a List with values in one line as:

        List<Integer> listOfInts = Arrays.asList(1, 2, 3);
       
        System.out.println(cities);
        
        
    }
    
}
