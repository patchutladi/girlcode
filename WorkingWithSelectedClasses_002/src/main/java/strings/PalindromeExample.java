/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package strings;

/**
 *
 * @author pertuniatladi
 */
public class PalindromeExample {

    /**
     * @param args the command line arguments
     */
    
    //A palindrome is a word or sentence that is symmetric—it is spelled the same forward and backward
    public static void main(String[] args) {
        // TODO code application logic here
        String palindrome = "Dot saw I was Tod";
        int len = palindrome.length();
        
        //Example of concatenation
        System.out.println("Length of palindrome" .concat(palindrome) + len);
        
        // iunitialize  array of chars
        char[] tempCharArray = new char[len];
        char[] charArray = new char[len];
        
        // put original string in an array of chars
        for (int i = 0; i < len; i++) {
            tempCharArray[i] = 
                palindrome.charAt(i);
        } 
        
        // reverse array of chars
        for (int j = 0; j < len; j++) {
            charArray[j] =
                tempCharArray[len - 1 - j];
        }
        
        String reversePalindrome =
            new String(charArray);
        System.out.println(reversePalindrome);
        
        
        //Format Strings
        String fs;
        float floatVar = 5.8f;
        int intVar = 5;
        String stringVar = "5";
        fs = String.format("The value of the float " +
                   "variable is %f, while " +
                   "the value of the " + 
                   "integer variable is %d, " +
                   " and the string is %s",
                   floatVar, intVar, stringVar);
        System.out.println(fs);
    }
    
}
