/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lambda;

/**
 *
 * @author pertuniatladi
 */
public class LambdaExample_002 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Arrow Token informs the compiler that this expression is a lambda notation and parse it accordingly
        SomeListener listener = (a,b) -> System.out.println("Action Performed" + a + b);
	        listener.action(9,10);
                
                //Body is the actual implementation of the functional method. Either this could be a single expression,
                //single statement without curly braces, or multiple statements with curly braces.

    }
   
    interface SomeListener {
        //Argument List is the parameters of the method
	    void action(int x, int y);	}

}

