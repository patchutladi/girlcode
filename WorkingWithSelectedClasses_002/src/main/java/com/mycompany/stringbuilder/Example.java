/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.stringbuilder;

/**
 *
 * @author pertuniatladi
 */
public class Example {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Append
        StringBuilder strSB = new StringBuilder("This is an example of ");
        strSB.append("append method.");
        System.out.println(strSB);
        
        //Insert
        StringBuilder strSB_002 = new StringBuilder("This is a program");
        strSB_002.insert(10, "java ");
        System.out.println(strSB_002);  
        
        //Replace
        StringBuilder strSB_003 = new StringBuilder("This is a program");
        strSB_003.replace(0, 9, "java");
        System.out.println(strSB_003);  
        
        //Delete
        StringBuilder strSB_004 = new StringBuilder("This is a program");
        strSB_004.delete(0, 10);
        System.out.println(strSB_004);  
        
        //Reverse
        StringBuilder strSB_005 = new StringBuilder("This is a program");
        strSB_005.reverse();
        System.out.println(strSB_005);
        
    }

}
