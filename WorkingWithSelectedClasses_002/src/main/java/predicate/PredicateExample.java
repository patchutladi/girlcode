/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package predicate;
import java.util.*;
/**
 *
 * @author pertuniatladi
 */
public class PredicateExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Filter through all the ages of the children and show only those above age 8
                Child child1 = new Child(3);
	        Child child2 = new Child(2);
	        Child child3 = new Child(7);
	        Child child4 = new Child(10);
	        Child child5 = new Child(6);
	        Child child6 = new Child(9);
	        Child child7 = new Child(8);
                
                
	        List<Child> childs = Arrays.asList(new Child[] { child1, child2,
	                child3, child4, child5, child6, child7 });
	        List<Child> filtered = ChildPredicates.filterChilds(childs,
	                ChildPredicates.filterByAge(8));
	        for (Child child : filtered) {
	            System.out.println(child.getAge());
	        }
	    }
	}

    

