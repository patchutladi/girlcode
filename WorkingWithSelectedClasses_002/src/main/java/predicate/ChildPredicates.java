/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package predicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.List;

/**
 *
 * @author pertuniatladi
 */
public class ChildPredicates {
    
      static Predicate<Child> filterByAge(int x) {
	        return a -> a.getAge() > x;
	    }
	 // Predicates represent single-argument functions that return a boolean value.
	    static List<Child> filterChilds(List<Child> childs,
	            Predicate<Child> predicate) {
	        return childs.stream().filter(predicate)
	                .collect(Collectors.<Child> toList());
	    }

}
